$(document).ready(function() {

  $('.phone').inputmask('+7(999)-999-99-99');

  $('.open-nav').on('click', function(e) {
    e.preventDefault();
    $('.r-nav').toggle();
  });

  $('.r-nav__close').on('click', function(e) {
    e.preventDefault();
    $('.r-nav').toggle();
  });

  $('.open-modal').on('click', function(e) {
    e.preventDefault();
    $('.r-modal').toggle();
  });

  $('.r-modal__close').on('click', function(e) {
    e.preventDefault();
    $('.r-modal').toggle();
  });

  new Swiper('.r-home-events__cards', {
    slidesPerView: 'auto',
    spaceBetween: 30,
    loop: true,
    resizeReInit: true,
    navigation: {
      nextEl: '.r-home-events__cards .swiper-button-next',
      prevEl: '.r-home-events__cards .swiper-button-prev',
    },
    breakpoints: {
      1200: {
        slidesPerView: 'auto',
        spaceBetween: 30,
      }
    }
  });

  new Swiper('.r-home-gallery__cards_1', {
    slidesPerView: 'auto',
    spaceBetween: 30,
    freeMode: true,
    loop: true,
    resizeReInit: true,
    centerMode: true,
    /* autoplay: {
      delay: 1,
    },
    speed: 5000, */
  });

  new Swiper('.r-home-gallery__cards_2', {
    slidesPerView: 'auto',
    spaceBetween: 30,
    freeMode: true,
    loop: true,
    resizeReInit: true,
    centerMode: true,
    /* autoplay: {
      delay: 1,
    },
    speed: 5000, */
  });

  new Swiper('.r-home-areas__cards', {
    slidesPerView: 2,
    spaceBetween: 30,
    loop: true,
    resizeReInit: true,
    navigation: {
      nextEl: '.r-home-areas__next',
      prevEl: '.r-home-areas__prev',
    },
    breakpoints: {
      1200: {
        spaceBetween: 0,
        slidesPerView: 1
      }
    }
  });

  new Swiper('.r-big-gallery__cards', {
    slidesPerView: 'auto',
    spaceBetween: 30,
    loop: true,
    resizeReInit: true,
    navigation: {
      nextEl: '.r-big-gallery__cards .swiper-button-next',
      prevEl: '.r-big-gallery__cards .swiper-button-prev',
    },
    breakpoints: {
      1200: {
        slidesPerView: 1
      }
    }
  });

  $('.r-bottom__up').click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
    return false;
  });

  $('.r-area__tabitem').click(function() {
    var tab_id = $(this).attr('data-tab');

    $('.r-area__tabitem').removeClass('r-area__tabitem_active');
    $('.r-area__tabcontent').removeClass('r-area__tabcontent_active');

    $(this).addClass('r-area__tabitem_active');
    $('#' + tab_id).addClass('r-area__tabcontent_active');
  });

  $('.r-view-main__object').on('click', function(e) {
    e.preventDefault();

    if ($(this).find('.r-view-main__line').hasClass('r-view-main__line_active')) {
      $(this).find('.r-view-main__line').toggleClass('r-view-main__line_active');
      $(this).find('.r-view-main__flag').toggleClass('r-view-main__flag_active');
    } else {
      $('.r-view-main__line').removeClass('r-view-main__line_active');
      $('.r-view-main__flag').removeClass('r-view-main__flag_active');
      $(this).find('.r-view-main__line').toggleClass('r-view-main__line_active');
      $(this).find('.r-view-main__flag').toggleClass('r-view-main__flag_active');
    }
  });

  var galleryThumbs = new Swiper('.r-resident__small', {
    spaceBetween: 15,
    slidesPerView: 3,
    freeMode: true,
    resizeReInit: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  var galleryTop = new Swiper('.r-resident__big', {
    spaceBetween: 10,
    thumbs: {
      swiper: galleryThumbs
    }
  });

  var galleryThumbs2 = new Swiper('.r-timeline__years', {
    spaceBetween: 20,
    slidesPerView: 'auto',
    resizeReInit: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  });
  var galleryTop2 = new Swiper('.r-timeline__cards', {
    slidesPerView: 'auto',
    spaceBetween: 100,
    freeMode: true,
    resizeReInit: true,
    mousewheel: true,
    thumbs: {
      swiper: galleryThumbs2
    }
  });

  $('.r-home-gallery__cards_1').paroller({
    factor: 0.3,
    factorXs: 0,
    factorSm: 0,
    type: 'foreground',
    direction: 'horizontal'
  });

  $('.r-home-gallery__cards_2').paroller({
    factor: 0.5,
    factorXs: 0,
    factorSm: 0,
    type: 'foreground',
    direction: 'horizontal'
  });

  $(window).on('load', function() {
    if ($(window).width() > 1200) {
      new WOW().init();
    }
  });

  $(window).on('load resize scroll', function() {
    if ($(window).scrollTop() > 150) {
      $('.r-header').addClass('r-header_active');
    } else {
      $('.r-header').removeClass('r-header_active');
    }
  });
});